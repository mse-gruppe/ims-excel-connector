table 5563351 "IMS Excel Mat. Imp. Buffer"
{
    Caption = 'Quote Matching Buffer';
    DataClassification = ToBeClassified;

    fields
    {
        field(11; "Part No."; Code[20])
        {
            Caption = 'Part No.';
            DataClassification = ToBeClassified;
        }
        field(12; "Item No."; Code[20])
        {
            Caption = 'Item No.';
            DataClassification = ToBeClassified;
        }
        field(13; Quantity; Decimal)
        {
            Caption = 'Quantity';
            DataClassification = ToBeClassified;
        }
        field(14; "Price EUR"; Decimal)
        {
            Caption = 'Price EUR';
            DataClassification = ToBeClassified;
        }
        field(15; "Line Amount"; Decimal)
        {
            Caption = 'Line Amount';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; "Part No.")
        {
            Clustered = true;
        }
    }

}
