table 5563353 "IMS Excel Parts Calc. Buffer"
{
    Caption = 'IMS Parts Calculation Buffer';
    DataClassification = ToBeClassified;

    fields
    {
        field(11; "Part No."; Code[20])
        {
            Caption = 'Part No.';
            DataClassification = ToBeClassified;
        }
        field(12; Quantity; Decimal)
        {
            Caption = 'Quantity';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; "Part No.")
        {
            Clustered = true;
        }
    }

}
