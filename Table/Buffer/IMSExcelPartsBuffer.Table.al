table 5563352 "IMS Excel Parts Buffer"
{
    Caption = 'IMS Parts Buffer';
    DataClassification = ToBeClassified;

    fields
    {
        field(11; "Part No."; Code[20])
        {
            Caption = 'Part No.';
            DataClassification = ToBeClassified;
        }
        field(12; "Price EUR"; Decimal)
        {
            Caption = 'Price EUR';
            DataClassification = ToBeClassified;
        }
        field(13; "Core Price EUR"; Decimal)
        {
            Caption = 'Core Price EUR';
            DataClassification = ToBeClassified;
        }
        field(14; "Designation (german)"; Text[250])
        {
            Caption = 'Designation (german)';
            DataClassification = ToBeClassified;
        }
        field(15; Length; Integer)
        {
            Caption = 'Length';
            DataClassification = ToBeClassified;
        }
        field(16; "Designation (english)"; Text[250])
        {
            Caption = 'Designation (english)';
            DataClassification = ToBeClassified;
        }
        field(17; "Designation (french)"; Text[250])
        {
            Caption = 'Designation (french)';
            DataClassification = ToBeClassified;
        }
        field(18; "Designation (portugues)"; Text[250])
        {
            Caption = 'Designation (portugues)';
            DataClassification = ToBeClassified;
        }
        field(19; "Designation (spanish)"; Text[250])
        {
            Caption = 'Designation (spanish)';
            DataClassification = ToBeClassified;
        }
        field(20; "Designation (italian)"; Text[250])
        {
            Caption = 'Designation (italian)';
            DataClassification = ToBeClassified;
        }

        field(21; "CAN (Customs assigned number)"; Text[250])
        {
            Caption = 'CAN (Customs assigned number)';
            DataClassification = ToBeClassified;
        }
        field(22; Dimensions; Text[250])
        {
            Caption = 'Dimensions';
            DataClassification = ToBeClassified;
        }
        field(23; Norm; Text[250])
        {
            Caption = 'Norm';
            DataClassification = ToBeClassified;
        }
        field(24; "Unit Weight"; Decimal)
        {
            Caption = 'Unit Weight';
            DataClassification = ToBeClassified;
        }
        field(25; "Unit of Weight"; Text[250])
        {
            Caption = 'Unit of Weight';
            DataClassification = ToBeClassified;
        }
        field(26; "Base Unit of Measure"; Code[10])
        {
            Caption = 'Base Unit of Measure';
            DataClassification = ToBeClassified;
            TableRelation = "Unit of Measure";
        }
        field(27; "PRDH"; Text[250])
        {
            Caption = 'PRDH';
            DataClassification = ToBeClassified;
        }
        field(28; "NATO-VERS.-NR."; Text[250])
        {
            Caption = 'NATO-VERS.-NR.';
            DataClassification = ToBeClassified;
        }
        field(29; "COLT UB (days)"; Text[250])
        {
            Caption = 'COLT UB (days)';
            DataClassification = ToBeClassified;
        }
        field(30; "Effective Date"; Date)
        {
            Caption = 'Effective Date';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; "Part No.")
        {
            Clustered = true;
        }
    }

}
