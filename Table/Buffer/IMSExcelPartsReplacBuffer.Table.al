table 5563354 "IMS Excel Parts Replac. Buffer"
{
    Caption = 'Parts Replacement Buffer';
    DataClassification = ToBeClassified;

    fields
    {
        field(11; "Date"; Date)
        {
            Caption = 'Date';
            DataClassification = ToBeClassified;
        }
        field(12; "Replacement Part No."; Code[20])
        {
            Caption = 'Replacement Part No.';
            DataClassification = ToBeClassified;
        }
        field(13; "Part No."; Code[20])
        {
            Caption = 'Part No.';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; "Part No.")
        {
            Clustered = true;
        }
    }

}
