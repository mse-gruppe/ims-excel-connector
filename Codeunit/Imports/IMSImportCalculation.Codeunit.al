Codeunit 5563351 "IMS Import Calculation"
{
    TableNo = "mse365 Import Line";

    trigger OnRun()
    begin
        LineTransferFields(Rec);
    end;

    procedure CreateAndShowImportHeaderFromCalculation(var CalculationHeader: Record "IMS Calculation Header")
    var
        ImportHeader: Record "mse365 Import Header";
    begin
        CreateNewImportHeaderFromCalculation(CalculationHeader, ImportHeader);
        FilterImportHeaderForCalculation(CalculationHeader, ImportHeader);
        Page.Run(Page::"mse365 Import Card", ImportHeader);
    end;

    procedure CreateNewImportHeaderFromCalculation(var CalculationHeader: Record "IMS Calculation Header"; var ImportHeader: Record "mse365 Import Header"): Boolean
    begin
        if CalculationHeader."Import No." <> '' then
            exit(false);

        with ImportHeader do begin
            Init();
            Validate("Import Type Code", GetImportTypeCodeCalculation());
            Insert(true);
            CalculationHeader.Validate("Import No.", "No.");
            exit(true);
        end;
    end;

    local procedure GetImportTypeCodeCalculation(): Code[10]
    var
        ImportType: Record "mse365 Import Type";
    begin
        with ImportType do begin
            SetRange("Processing Codeunit Id", Codeunit::"IMS Import Calculation");
            FindFirst();
            exit(Code);
        end;
    end;

    local procedure FilterImportHeaderForCalculation(CalculationHeader: Record "IMS Calculation Header"; var ImportHeader: Record "mse365 Import Header")
    begin
        with ImportHeader do begin
            SetRange("Import Type Code", GetImportTypeCodeCalculation());
            SetRange("No.", CalculationHeader."Import No.");
        end;
    end;

    procedure LineTransferFields(var IMSImportLine: Record "mse365 Import Line")
    var
        IMSPartsCalculationBuffer: Record "IMS Excel Parts Calc. Buffer";
        IMSCalculationHeader: Record "IMS Calculation Header";
    begin
        EvaluateFieldValues(IMSPartsCalculationBuffer, IMSImportLine);
        CreateFindIMSCalculationHeader(IMSCalculationHeader, IMSImportLine, IMSImportLine."Check Only");
        CreateIMSCalculationLines(IMSCalculationHeader, IMSPartsCalculationBuffer, IMSImportLine."Check Only");
    end;

    local procedure EvaluateFieldValues(var IMSPartsCalculationBuffer: Record "IMS Excel Parts Calc. Buffer"; IMSImportLine: Record "mse365 Import Line")
    begin
        with IMSPartsCalculationBuffer do begin
            Evaluate(IMSPartsCalculationBuffer."Part No.", IMSImportLine."Text Value 1");
            IMSEvaluateFieldValues.EvaluateTextToFieldDecimal(Quantity, IMSImportLine."Text Value 2");
        end;
    end;

    local procedure CreateFindIMSCalculationHeader(var IMSCalculationHeader: Record "IMS Calculation Header"; IMSImportLine: Record "mse365 Import Line"; CheckOnly: Boolean)
    begin
        with IMSCalculationHeader do begin
            Setrange("Import No.", IMSImportLine."mse365 Import No.");
            if FindFirst() then
                exit;

            Reset();
            Init();
            Validate("Document Type", IMSCalculationHeader."Document Type"::Calculation);
            Validate("Import No.", IMSImportLine."mse365 Import No.");

            if CheckOnly then
                exit;

            Insert(true);
        end;
    end;

    local procedure CreateIMSCalculationLines(IMSCalculationHeader: Record "IMS Calculation Header"; IMSPartsCalculationBuffer: Record "IMS Excel Parts Calc. Buffer"; CheckOnly: Boolean)
    var
        IMSCalculationLine: Record "IMS Calculation Line";
        LastIMSCalculationLine: Record "IMS Calculation Line";
    begin
        with IMSCalculationLine do begin
            LastIMSCalculationLine.SetRange("Document Type", IMSCalculationHeader."Document Type");
            LastIMSCalculationLine.SetRange("Document No.", IMSCalculationHeader."No.");
            if LastIMSCalculationLine.FindLast() then;

            Init();
            Validate("Document Type", IMSCalculationHeader."Document Type");
            Validate("Document No.", IMSCalculationHeader."No.");
            Validate("Line No.", LastIMSCalculationLine."Line No." + 10000);
            Validate(Type, Type::Parts);
            Validate("No.", IMSPartsCalculationBuffer."Part No.");
            Validate(Quantity, IMSPartsCalculationBuffer.Quantity);

            if CheckOnly then
                exit;

            Insert();
            IndentationMgt.ExtendItemLinesForPartLine(IMSCalculationLine);
        end;
    end;

    var
        IMSEvaluateFieldValues: Codeunit "mse365 Evaluate Field Values";
        IndentationMgt: Codeunit "IMS Indentation Mgt.";
}
