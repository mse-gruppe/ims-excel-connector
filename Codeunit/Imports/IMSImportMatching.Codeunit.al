codeunit 5563353 "IMS Import Matching"
{
    TableNo = "mse365 Import Line";

    trigger OnRun()
    begin
        LineTransferFields(Rec);
    end;

    procedure CreateAndShowImportHeaderFromMatching(var Matching: Record "IMS Matching")
    var
        ImportHeader: Record "mse365 Import Header";
    begin
        CreateNewImportHeaderFromMatching(Matching, ImportHeader);
        FilterImportHeaderForMatching(Matching, ImportHeader);
        Page.Run(Page::"mse365 Import Card", ImportHeader);
    end;

    procedure CreateNewImportHeaderFromMatching(var Matching: Record "IMS Matching"; var ImportHeader: Record "mse365 Import Header"): Boolean
    begin
        if Matching."Import No." <> '' then
            exit(false);

        Matching.TestField("Import No.", '');
        Matching.TestField("Matching Type");

        ImportHeader.Init();
        ImportHeader.Validate("Import Type Code", GetImportTypeCodeMatching(Matching));
        ImportHeader.Insert(true);
        Matching.Validate("Import No.", ImportHeader."No.");
        exit(true);
    end;

    local procedure GetImportTypeCodeMatching(Matching: Record "IMS Matching"): Code[10]
    var
        ImportType: Record "mse365 Import Type";
    begin
        Matching.TestField("Matching Type");
        case Matching."Matching Type" of
            Matching."Matching Type"::"Quote Confirmation":
                ImportType.SetRange(Code, QuoteMatchTok);
            Matching."Matching Type"::"Purchase Call":
                ImportType.SetRange(Code, OrderMatchTok);
        end;

        ImportType.SetRange("Processing Codeunit Id", Codeunit::"IMS Import Matching");
        ImportType.FindFirst();
        exit(ImportType.Code);
    end;

    local procedure FilterImportHeaderForMatching(Matching: Record "IMS Matching"; var ImportHeader: Record "mse365 Import Header")
    begin
        ImportHeader.SetRange("Import Type Code", GetImportTypeCodeMatching(Matching));
        ImportHeader.SetRange("No.", Matching."Import No.");
    end;

    procedure LineTransferFields(var IMSImportLine: Record "mse365 Import Line")
    var
        MatchingImpBuffer: Record "IMS Excel Mat. Imp. Buffer";
    begin
        EvaluateFieldValues(MatchingImpBuffer, IMSImportLine);
        CreateMatchingLines(MatchingImpBuffer, IMSImportLine);
    end;

    local procedure EvaluateFieldValues(var MatchingImpBuffer: Record "IMS Excel Mat. Imp. Buffer"; IMSImportLine: Record "mse365 Import Line")
    begin
        with MatchingImpBuffer do begin
            Evaluate("Part No.", IMSImportLine."Text Value 1");
            Evaluate("Item No.", IMSImportLine."Text Value 2");
            IMSEvaluateFieldValues.EvaluateTextToFieldDecimal(Quantity, IMSImportLine."Text Value 3");
            IMSEvaluateFieldValues.EvaluateTextToFieldDecimal("Price EUR", IMSImportLine."Text Value 4");
            IMSEvaluateFieldValues.EvaluateTextToFieldDecimal("Line Amount", IMSImportLine."Text Value 5");
        end;
    end;

    local procedure CreateMatchingLines(MatchingImpBuffer: Record "IMS Excel Mat. Imp. Buffer"; ImportLine: Record "mse365 Import Line")
    var
        Matching: Record "IMS Matching";
    begin
        CreateFindIMSMatching(Matching, ImportLine);
        CreateMatchingLines(MatchingImpBuffer, ImportLine, Matching);
    end;

    local procedure CreateFindIMSMatching(var Matching: Record "IMS Matching"; ImportLine: Record "mse365 Import Line")
    var
        ImportHeader: Record "mse365 Import Header";
    begin
        Matching.SetRange("Import No.", ImportLine."mse365 Import No.");
        if Matching.FindFirst() then
            exit;

        ImportHeader.Get(ImportLine."mse365 Import No.");

        Matching.Reset();
        Matching.Init();

        case ImportHeader."Import Type Code" of
            QuoteMatchTok:
                Matching.Validate("Matching Type", Matching."Matching Type"::"Quote Confirmation");
            OrderMatchTok:
                Matching.Validate("Matching Type", Matching."Matching Type"::"Purchase Call");
        end;

        Matching.Validate("Import No.", ImportLine."mse365 Import No.");

        if ImportLine."Check Only" then
            exit;

        Matching.Insert(true);
    end;

    local procedure GetNextMatchingLine(Matching: Record "IMS Matching"): Integer
    var
        MatchingLine: Record "IMS Matching Line";
    begin
        MatchingLine.SetRange("Matching Type", Matching."Matching Type");
        MatchingLine.SetRange("Matching No.", Matching."No.");
        if MatchingLine.FindLast() then;
        exit(MatchingLine."Line No." + 10000);
    end;

    local procedure TestFields(var MatchingImpBuffer: Record "IMS Excel Mat. Imp. Buffer")
    begin
        with MatchingImpBuffer do begin
            TestField("Part No.");
            TestField("Item No.");
            TestField(Quantity);
        end;
    end;

    local procedure FindInitMatching(var Matching: Record "IMS Matching"; ImportLine: Record "mse365 Import Line")
    begin
        Matching.SetRange("Import No.", ImportLine."mse365 Import No.");
        if Matching.FindFirst() then
            exit;

        Matching.InitInsert();
        Matching.Validate("Matching Type", Matching."Matching Type"::"Quote Confirmation");
    end;

    local procedure CreateMatchingLines(var MatchingImpBuffer: Record "IMS Excel Mat. Imp. Buffer"; var ImportLine: Record "mse365 Import Line"; var Matching: Record "IMS Matching")
    var
        MatchingLine: Record "IMS Matching Line";
    begin
        MatchingLine.Init();
        MatchingLine.Validate("Matching Type", Matching."Matching Type");
        MatchingLine.Validate("Matching No.", Matching."No.");
        MatchingLine.Validate("Line No.", GetNextMatchingLine(Matching));

        MatchingLine.Validate("Part No.", MatchingImpBuffer."Part No.");
        MatchingLine.Validate("Item No.", MatchingImpBuffer."Item No.");
        MatchingLine.Validate(Quantity, MatchingImpBuffer.Quantity);
        MatchingLine.Validate("Price EUR", MatchingImpBuffer."Price EUR");
        MatchingLine.Validate("Line Amount", MatchingImpBuffer."Line Amount");

        if ImportLine."Check Only" then
            exit;

        MatchingLine.Insert();
    end;

    var
        IMSEvaluateFieldValues: Codeunit "mse365 Evaluate Field Values";
        QuoteMatchTok: Label 'QUOTEMATCH', Locked = true;
        OrderMatchTok: Label 'ORDERMATCH', Locked = true;
}
