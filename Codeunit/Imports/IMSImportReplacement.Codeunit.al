codeunit 5563355 "IMS Import Replacement"
{
    TableNo = "mse365 Import Line";

    trigger OnRun()
    begin
        LineTransferFields(Rec);
    end;

    procedure LineTransferFields(var IMSImportLine: Record "mse365 Import Line")
    var
        PartsReplacementBuffer: Record "IMS Excel Parts Replac. Buffer";
    begin
        EvaluateFieldValues(PartsReplacementBuffer, IMSImportLine);
        CreateUpdatePart(IMSImportLine, PartsReplacementBuffer);
    end;

    local procedure EvaluateFieldValues(var PartsReplacementBuffer: Record "IMS Excel Parts Replac. Buffer"; IMSImportLine: Record "mse365 Import Line")
    begin
        Evaluate(PartsReplacementBuffer.Date, IMSImportLine."Text Value 1");
        Evaluate(PartsReplacementBuffer."Replacement Part No.", IMSImportLine."Text Value 2");
        Evaluate(PartsReplacementBuffer."Part No.", IMSImportLine."Text Value 3");
    end;

    local procedure CreateUpdatePart(IMSImportLine: Record "mse365 Import Line"; PartsReplacementBuffer: Record "IMS Excel Parts Replac. Buffer")
    var
        IMSPart: Record "IMS Part";
    begin
        with IMSPart do begin
            Get(PartsReplacementBuffer."Part No.");
            Validate("Replacement No.", PartsReplacementBuffer."Replacement Part No.");

            if IMSImportLine."Check Only" then
                exit;

            Modify(true);
        end;
    end;
}
