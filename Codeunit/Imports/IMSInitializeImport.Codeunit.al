codeunit 5563356 "IMS Initialize Import"
{
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"mse365 Initialize Import", 'OnBeforeInitImport', '', false, false)]
    local procedure OnBeforeInitImportInitIMSImport()
    begin
        InitImport();
    end;

    procedure InitImport()
    begin
        ImportType.CreateNew('CALC', 'Kalkulation', Database::"IMS Excel Parts Calc. Buffer", Codeunit::"IMS Import Calculation", false);
        ImportType.CreateNew('PART', 'Teile', Database::"IMS Excel Parts Buffer", Codeunit::"IMS Import Parts", true);
        ImportType.CreateNew('REPLACE', 'Ersetzungen', Database::"IMS Excel Parts Replac. Buffer", Codeunit::"IMS Import Replacement", true);
        ImportType.CreateNew('QUOTEMATCH', 'Anfrage Zuordnungen', Database::"IMS Excel Mat. Imp. Buffer", Codeunit::"IMS Import Matching", true);
        ImportType.CreateNew('ORDERMATCH', 'Bestellung Zuordnungen', Database::"IMS Excel Mat. Imp. Buffer", Codeunit::"IMS Import Matching", true);
    end;

    var
        ImportType: Record "mse365 Import Type";
}