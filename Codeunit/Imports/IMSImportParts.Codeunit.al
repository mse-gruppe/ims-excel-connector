codeunit 5563354 "IMS Import Parts"
{
    TableNo = "mse365 Import Line";

    trigger OnRun()
    begin
        LineTransferFields(Rec);
    end;

    procedure LineTransferFields(var IMSImportLine: Record "mse365 Import Line")
    var
        IMSPartsBuffer: Record "IMS Excel Parts Buffer";
    begin
        EvaluateFieldValues(IMSPartsBuffer, IMSImportLine);
        CreateUpdatePart(IMSImportLine, IMSPartsBuffer);
    end;

    local procedure EvaluateFieldValues(var IMSPartsBuffer: Record "IMS Excel Parts Buffer"; IMSImportLine: Record "mse365 Import Line")
    begin
        with IMSPartsBuffer do begin
            Evaluate("Part No.", IMSImportLine."Text Value 1");
            IMSEvaluateFieldValues.EvaluateTextToFieldDecimal("Price EUR", IMSImportLine."Text Value 2");
            IMSEvaluateFieldValues.EvaluateTextToFieldDecimal("Core Price EUR", IMSImportLine."Text Value 3");
            Evaluate("Designation (german)", IMSImportLine."Text Value 4");
            IMSEvaluateFieldValues.EvaluateTextToFieldInteger(Length, IMSImportLine."Text Value 5");
            Evaluate("Designation (english)", IMSImportLine."Text Value 6");
            Evaluate("Designation (french)", IMSImportLine."Text Value 7");
            Evaluate("Designation (portugues)", IMSImportLine."Text Value 8");
            Evaluate("Designation (spanish)", IMSImportLine."Text Value 9");
            Evaluate("Designation (italian)", IMSImportLine."Text Value 10");
            Evaluate("CAN (Customs assigned number)", IMSImportLine."Text Value 11");
            Evaluate(Dimensions, IMSImportLine."Text Value 12");
            Evaluate(Norm, IMSImportLine."Text Value 13");
            IMSEvaluateFieldValues.EvaluateTextToFieldDecimal("Unit Weight", IMSImportLine."Text Value 14");
            Evaluate("Unit of Weight", IMSImportLine."Text Value 15");
            Evaluate("Base Unit of Measure", IMSImportLine."Text Value 16");
            Evaluate("PRDH", IMSImportLine."Text Value 17");
            Evaluate("NATO-VERS.-NR.", IMSImportLine."Text Value 18");
            Evaluate("COLT UB (days)", IMSImportLine."Text Value 19");
            IMSEvaluateFieldValues.EvaluateTextToFieldDate("Effective Date", IMSImportLine."Text Value 20");
        end;
    end;

    local procedure CreateUpdatePart(IMSImportLine: Record "mse365 Import Line"; IMSPartsBuffer: Record "IMS Excel Parts Buffer")
    var
        IMSPart: Record "IMS Part";
        UnitOfMeasure: Record "Unit of Measure";
    begin
        with IMSPart do begin
            if not Get(IMSPartsBuffer."Part No.") then begin
                Init();
                Validate("No.", IMSPartsBuffer."Part No.");
                if not IMSImportLine."Check Only" then
                    Insert();
            end;

            if IMSPartsBuffer."Base Unit of Measure" <> '' then
                UnitOfMeasure.Get(IMSPartsBuffer."Base Unit of Measure");

            Validate("Unit Price", IMSPartsBuffer."Price EUR");
            Validate("Core Price", IMSPartsBuffer."Core Price EUR");
            Validate(Description, IMSPartsBuffer."Designation (german)");
            Validate("Base Unit of Measurement", IMSPartsBuffer."Base Unit of Measure");
            Validate("Custom Assigned No.", IMSPartsBuffer."CAN (Customs assigned number)");
            Validate(Measurements, IMSPartsBuffer.Dimensions);
            Validate(Norm, IMSPartsBuffer.Norm);
            Validate(Weight, IMSPartsBuffer."Unit Weight");
            Validate("Unit of Weight", IMSPartsBuffer."Unit of Weight");
            Validate(PRDH, IMSPartsBuffer.PRDH);
            Validate("Nato Version No.", IMSPartsBuffer."NATO-VERS.-NR.");
            Validate("Cold UB (Days)", IMSPartsBuffer."COLT UB (days)");
            Validate("Effective Date", IMSPartsBuffer."Effective Date");

            if IMSImportLine."Check Only" then
                exit;

            if not Insert(true) then
                Modify(true);
        end;
    end;

    var
        IMSEvaluateFieldValues: Codeunit "mse365 Evaluate Field Values";
}
