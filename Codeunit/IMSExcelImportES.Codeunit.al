codeunit 5563357 "IMS Excel Import ES"
{
    [EventSubscriber(ObjectType::Page, Page::"mse365 Import DropZone Part", 'OnBeforeImportHeaderSaveFileImportNew', '', false, false)]
    local procedure OnBeforeImportHeaderSaveFileImportNewCreateImport(var ImportHeaderIsSet: Boolean; var ImportHeader: Record "mse365 Import Header"; SourceRecordVariantIsSet: Boolean; SourceRecordVariant: Variant; var OpenImportCard: Boolean)
    begin
        if ImportHeaderIsSet then
            exit;

        if not SourceRecordVariant.IsRecord() then
            exit;

        ImportHeaderIsSet := CreateImportHeader(ImportHeader, SourceRecordVariant);
        OpenImportCard := ImportHeaderIsSet;
    end;

    local procedure CreateImportHeader(var ImportHeader: Record "mse365 Import Header"; SourceRecordVariant: Variant): Boolean
    var
        Matching: Record "IMS Matching";
        CalculationHeader: Record "IMS Calculation Header";
        RecRef: RecordRef;
    begin
        RecRef.GetTable(SourceRecordVariant);

        case RecRef.Number of
            Database::"IMS Matching":
                begin
                    RecRef.SetTable(Matching);
                    exit(CreateNewImportHeaderFromMatching(Matching, ImportHeader));
                end;
            Database::"IMS Calculation Header":
                begin
                    RecRef.SetTable(CalculationHeader);
                    exit(CreateNewImportHeaderFromCalculation(CalculationHeader, ImportHeader))
                end;
        end;

        exit(false);
    end;

    procedure CreateNewImportHeaderFromMatching(var Matching: Record "IMS Matching"; var ImportHeader: Record "mse365 Import Header") Successful: Boolean
    begin
        Matching.TestField("Customer No.");
        Matching.TestField("Import No.", '');
        Successful := ImportMatching.CreateNewImportHeaderFromMatching(Matching, ImportHeader);
        Matching.Modify(true);
        Commit();
    end;

    procedure CreateNewImportHeaderFromCalculation(var CalculationHeader: Record "IMS Calculation Header"; var ImportHeader: Record "mse365 Import Header") Successful: Boolean
    begin
        CalculationHeader.TestField("Sell-to Customer No.");
        CalculationHeader.TestField("Import No.", '');
        Successful := ImportCalculation.CreateNewImportHeaderFromCalculation(CalculationHeader, ImportHeader);
        CalculationHeader.Modify(true);
        Commit();
    end;

    var
        ImportMatching: Codeunit "IMS Import Matching";

        ImportCalculation: Codeunit "IMS Import Calculation";
}