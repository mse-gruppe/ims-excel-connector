pageextension 5563351 "IMS Excel Calculation" extends "IMS Calculation"
{
    layout
    {
        addlast(General)
        {
            field("Import No."; "Import No.")
            {
                ApplicationArea = All;
                Importance = Additional;
            }
        }

        addfirst(FactBoxes)
        {
            part(DropZone; "mse365 Import DropZone Part")
            {
            }
        }
    }

    actions
    {
        addafter(CreatePurchaseQuoteForRequest)
        {
            action(ImportPartLines)
            {
                Caption = 'Import Part Lines';
                Image = ImportExcel;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                ToolTip = 'Imports part lines from an excel file.';

                trigger OnAction()
                var
                    ImportCalculation: Codeunit "IMS Import Calculation";
                begin
                    ImportCalculation.CreateAndShowImportHeaderFromCalculation(Rec);
                end;
            }
        }
    }

    trigger OnModifyRecord(): Boolean
    begin
        UpdateDropZones();
    end;

    trigger OnAfterGetRecord()
    begin
        UpdateDropZones();
    end;

    trigger OnAfterGetCurrRecord()
    begin
        UpdateDropZones();
    end;

    local procedure UpdateDropZones()
    begin
        CurrPage.DropZone.Page.SetSourceRecordVariant(Rec);
        CurrPage.ArchiveDropZone.Page.SetRecordIdAndProcessNo(RecordId, "Process No.", "No." <> '');
        CurrPage.ArchiveDropZone.Page.Update();
    end;
}