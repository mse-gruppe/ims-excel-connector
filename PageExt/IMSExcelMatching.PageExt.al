pageextension 5563352 "IMS Excel Matching" extends "IMS Matching"
{
    layout
    {
        addafter("Customer No.")
        {
            field("Import No."; "Import No.")
            {
                ApplicationArea = All;
                Importance = Additional;
            }
        }

        addfirst(FactBoxes)
        {
            part(DropZone; "mse365 Import DropZone Part")
            {
            }
        }
    }

    actions
    {
        addfirst(Processing)
        {
            action(ImportMatchingLines)
            {
                Caption = 'Import Matching Lines';
                Image = ImportExcel;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                ToolTip = 'Imports matching lines from an excel file.';

                trigger OnAction()
                var
                    ImportMatching: Codeunit "IMS Import Matching";
                begin
                    ImportMatching.CreateAndShowImportHeaderFromMatching(Rec);
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        CurrPage.DropZone.Page.SetSourceRecordVariant(Rec);
    end;

    trigger OnAfterGetCurrRecord()
    begin
        CurrPage.DropZone.Page.SetSourceRecordVariant(Rec);
    end;
}