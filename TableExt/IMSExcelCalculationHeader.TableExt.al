tableextension 5563352 "IMS Excel Calculation Header" extends "IMS Calculation Header"
{
    fields
    {
        field(5563351; "Import No."; Code[20])
        {
            Caption = 'Import No.';
            TableRelation = "mse365 Import Header";
            Editable = false;
        }
    }
}