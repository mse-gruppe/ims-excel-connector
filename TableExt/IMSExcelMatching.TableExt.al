tableextension 5563351 "IMS Excel Matching" extends "IMS Matching"
{
    fields
    {
        field(5563351; "Import No."; Code[20])
        {
            Caption = 'Import No.';
            TableRelation = "mse365 Import Header";
            Editable = false;
        }
    }
}